# Drupal 7 module

This module is for fixing issues in [Manual Crop](https://www.drupal.org/project/manualcrop) module.

### Fixed issues  
- If an image is uploaded with the same name as a before uploaded image, the cropper shows the
previous uploaded image from the browser cache.